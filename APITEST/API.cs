﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace APITEST
{
    public class API
    {
        public HttpStatusCode Post() // тип возвращаемого значения HttpStatusCode
        {
            RestClient client = new RestClient("http://3.133.100.46/login") //куда отправляю запрос
            {
                Timeout = 300000
            };
            RestRequest request = new RestRequest(Method.POST); //создаю запрос
           
            request.AddHeader("Content-Type", "aplication/json"); //название хедера и его значение
           
            
            Dictionary<string, string> body = new Dictionary<string, string>() //заполняю тело запроса (девтулс неторк)
            {
                { "login", "Ivan"},
                {"password", "qwert12345" }
            };
            request.AddJsonBody(body);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);// отправляю апи запрос и сохраняю в переменную респонс
           
            return response.StatusCode;
        }


        public Cookie ExtraCookie(RestResponse response, string cookieName) //метод,которые помогает извлечь куки из ответа нашего
        {
            Cookie res = null; //это область памяти для куки
            foreach (var cookie in response.Cookies) // обрабатываю все куки которые вернулись
                if (cookie.Name.Equals(cookieName)) // нахожу нужную куки по имени
                    res = new Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain);
            return res;
        }

        public AddFiles()
        {
            RestClient client = new RestClient("http://3.133.100.46/login") //???
            {
                Timeout = 300000
            };
            RestRequest request = new RestRequest(Method.POST);
            request.AddParameter("application/octet-stream", FileAsBytes, ParameterType.RequestBody);//FileAsBytes не видит ???
            Dictionary<string, string> body = new Dictionary<string, string>() //нужен ли тут???
            {
                { "login", "Ivan"},
                {"password", "qwert12345" }
            };

            request.AddJsonBody(body);
            request.RequestFormat = DataFormat.Json;//верно ли указала формат????

            IRestResponse response = client.Execute(request);// нужно ли???

            return response.StatusCode;
        }

    }
}

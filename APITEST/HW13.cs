﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace APITEST
{
    public class HW13
    {
        public HttpStatusCode Registration_Post()
        {
            RestClient client = new RestClient("https://superpupers.com/register/")
            {
                Timeout = 300000
            };

            RestRequest request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "text/html");
            Dictionary<string, string> body = new Dictionary<string, string>()
                 {
                { "login", "Anastasia"},
                {"email","mirosha.shop@gmail.com"},
                {"password", "Qwerty1234*" }
            };

            request.AddBody(body);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);
            return response.StatusCode;
        }



        public HttpStatusCode Post()
        {
            RestClient client = new RestClient("https://superpupers.com/register/")
            {
                Timeout = 300000
            };
            RestRequest request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "text/html");

            Dictionary<string, string> body = new Dictionary<string, string>()
                 {
                { "login", "Anastasia"},
                {"email","mirosha.shop@gmail.com"},
                {"password", "Qwerty1234*" }
            };
            request.AddBody(body);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);
            return response.StatusCode;
        }


        public HttpStatusCode ChangePersonalInfo_Put()
        {
            RestClient client = new RestClient("https://superpupers.com/account-edit/")
            {
                Timeout = 300000
            };
            RestRequest request = new RestRequest(Method.PUT);
            request.AddHeader("content-type", "text/html");
            Dictionary<string, string> body = new Dictionary<string, string>()
            {
                { "email", "mirosha.shop@gmail.com" },
                { "first name", "Cooper" },
                { "last name", "Lee" },
                { "phone", "0505005050" },
                { "password", "Qwerty1234" },
                { "confirm password", "Qwerty1234" }

            };
            request.AddBody(body);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);
            return response.StatusCode;


        }
    }
}
